import React, {useState} from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import './sidebar.css'

function Sidebar() {
    const [sidebarOpen, setSidebarOpen] = useState(true);
    const toggle = () => {
        if(sidebarOpen) {
            setSidebarOpen(false);
        } else {
            setSidebarOpen(true);
        }
    }
    const barStatus = `${sidebarOpen ? 'sidebarOpen' : 'sidebarClose'}`;
    return (
        <div className={barStatus}>
            <div className="inline">
                <h1>Swift By Sundell</h1>
                <h1><FontAwesomeIcon icon="bars" onClick={toggle} style={{color: 'orange'}}/></h1>
            </div>
            <div className="articles">
                <div className="content-header">
                    <h3><FontAwesomeIcon icon="newspaper"/></h3>
                    <h3>Articles</h3>
                    <h3><FontAwesomeIcon icon="angle-down" style={{color: 'orange'}} /></h3>
                </div>
                <div className="list-content">
                    <h4>Weekly</h4>
                    <h4>Tips</h4>
                    <h4>Basics</h4>
                    <h4>All</h4>
                </div>
            </div>
            <div className="podcast">
                <div className="content-header">
                    <h3><FontAwesomeIcon icon="headphones"/></h3>
                    <h3>Podcast</h3>
                    <h3><FontAwesomeIcon icon="angle-down" style={{color: 'orange'}} /></h3>
                </div>
                <div className="list-content">
                    <h4>Latest</h4>
                    <h4>All</h4>
                </div>
            </div>
            <div className="video">
                <div className="content-header">
                    <h3><FontAwesomeIcon icon="video"/></h3>
                    <h3>Videos</h3>
                    <h3><FontAwesomeIcon icon="angle-down" style={{color: 'orange'}} /></h3>
                </div>
                <div className="list-content">
                    <h4>Clips</h4>
                </div>
            </div>
            <div className="favourite">
                <div className="content-header">
                    <h3><FontAwesomeIcon icon="star" style={{color: 'orange'}}/></h3>
                    <h3>Favorites</h3>
                    <h3><FontAwesomeIcon icon="angle-down" style={{color: 'orange'}} /></h3>
                </div>
                <div className="list-content">
                    <div className="content-header">
                        <h5><FontAwesomeIcon icon="newspaper"/></h5>
                        <h5>Exploring Swift 5.2's new Features</h5>
                    </div>
                    <div className="content-header">
                        <h5><FontAwesomeIcon icon="headphones"/></h5>
                        <h5>Taylor Swift's new song</h5>
                    </div>
                    <div className="content-header">
                        <h5><FontAwesomeIcon icon="newspaper"/></h5>
                        <h5>Getting Most Out of Xcode</h5>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Sidebar