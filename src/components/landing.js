import React from 'react'
import './landing.scss'

const landing = () => {
    return (
      <div className="landing">
          <div className="menu-icon">
              <span className="menu-icon__line"></span>
              <span className="menu-icon__line menu-icon__line-left"></span>
          </div>

          <div className="nav">
              <div className="nav__content">
                  <ul className="nav__list">
                      <li className="nav__list-item">Home</li>
                      <li className="nav__list-item">About</li>
                      <li className="nav__list-item">Projects</li>
                      <li className="nav__list-item">Contact</li>
                  </ul>
              </div>
          </div>

          <div className="site-content">
              <h1 className="site-content__headline">Another menu concept</h1>
          </div>
      </div>
    )
}

export default landing