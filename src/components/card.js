import React from "react"
import './card.css'

const Card = () => (
    <div className='card'>
        <h3>A Swift Developer's Dreams</h3>
        <div className='tags'>
            <h6 style={{backgroundColor: `#e24141`}}>Before wwdc</h6>
            <h6 style={{backgroundColor: `#269329`}}>Wishlist</h6>
            <h6 style={{backgroundColor: `#754ace`}}>Prediction</h6>
        </div>
        <div className='text'>
           <h5>WIth just few hours to go until WWDC20 kicks off with the first ever online-only Apply Keynote, I thought i'd do the same thing as I did last year, and share a few of my biggest Swift-related dreams for this upcoming WWDC.</h5>
        </div>
    </div>
)


export default Card
