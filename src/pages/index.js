import React from "react"

import SEO from "../components/seo"
import './index.css'
import '../components/landing.scss'
import firstImage from '../images/1.png';
import secondImage from '../images/2.png';
import thirdImage from '../images/3.png';
import logo from '../images/logo.svg';
class BlogPage extends React.Component {
    componentDidMount() {
        let body;
        let menu;
        let menuItems;

        const init = () => {
            body = document.querySelector("body");
            menu = document.querySelector(".menu-icon");
            menuItems = document.querySelectorAll(".nav__list-item");
            applyListeners();
        };

        const applyListeners = () => {
            menu.addEventListener("click", () => toggleClass(body, "nav-active"));
            console.log('call');
        };

        function toggleClass(element, stringClass){
            if (element.classList.contains(stringClass))
                element.classList.remove(stringClass);
            else element.classList.add(stringClass);
        }
        init();
    }
    render() {
        return (
        <div>
            <layout className="content">
                <SEO title='Landing Page'/>
                <img src={logo} className="logo"/>
                <div className="menu-icon">
                    <span className="menu-icon__line"></span>
                    <span className="menu-icon__line menu-icon__line-left"></span>
                </div>
                <div className="nav">
                    <div className="nav__content">
                        <ul className="nav__list">
                            <li className="nav__list-item">Home</li>
                            <li className="nav__list-item">About</li>
                            <li className="nav__list-item">Projects</li>
                            <li className="nav__list-item">Contact</li>
                        </ul>
                    </div>
                </div>

                <div className="site-content">
                    <div className="header-grid">
                        <h1 className="site-content__headline" data-sal="slide-up" data-sal-delay="100" data-sal-duration="1200" data-sal-easing="ease">We help transportation<br/> companies to design<br/> efficient digital products</h1>
                        <h2 className="site-content__headline" data-sal="slide-up" data-sal-delay="100" data-sal-duration="1200" data-sal-easing="ease">We design web & mobile apps from <br className="sm-break"/> sketch to launch, improving them over<br className="sm-break"/> time. Our mission is to bring your product<br className="sm-break"/> to the whole new level by solving real<br className="sm-break"/> problems and reaching business goals.</h2>
                    </div>
                    <div className="images" data-sal="slide-up" data-sal-delay="100" data-sal-duration="1200" data-sal-easing="ease">
                        <div className="image-container" >
                            <img src={firstImage}/>
                        </div>
                        <div className="image-text" >
                            <h1>Platform for parking in airport</h1>
                            <h4>Make parking great again: quick booking and easy lots management</h4>
                        </div>
                    </div>
                    <div className="images" data-sal="slide-up" data-sal-delay="100" data-sal-duration="1200" data-sal-easing="ease">
                        <div className="image-container" >
                            <img src={secondImage}/>
                        </div>
                        <div className="image-text" >
                            <h1>Web app for fleet management 2.0</h1>
                            <h4>Building efficient platform for vehicle-centered business</h4>
                        </div>
                    </div>
                    <div className="images" data-sal="slide-up" data-sal-delay="100" data-sal-duration="1200" data-sal-easing="ease">
                        <div className="image-container" >
                            <img src={thirdImage}/>
                        </div>
                        <div className="image-text" >
                            <h1>Mobile App for Automotive Service</h1>
                            <h4>How we helped auto mechanics to find their customers</h4>
                        </div>
                    </div>
                </div>
            </layout>
        </div>
        )
    }
}
export default BlogPage
